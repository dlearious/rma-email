# RateMyPlacement Email Templates
Rebuild of rate templates to aid standarisation. Some model magic for Latest Vacancies and Grunt build/deploy and GIT for version control

## Mandrill
- Account activation not sent

## Todo List - Priority 1
- trio
- blog format

## Todo List - Priority 2
- Split up type.less if possible
- Latest Vacancies alt layout
- Multi image grid
- Top 10 Table/list
- Stats component
- Hero caption improvements
- Broken image test
- Constrict email width test
- yahoo stars

## Todo List - Priority 3
- Adjustable width in frame template

## Nice to Have
- S3 Deployment
- Write up detailed docs with markdown and <code>

## More Templates
- Generic marketing message (contiki?)
- Events email
- Review (all about law?)
- Spotlight 
- OCP template

## Future Ideas
- mobile first fork
- fixedewidth mobile fork
- RMA fork
- drop Outlook support
- change width to 600

## Requirements
- Node.js
	- grunt
	- grunt-cli
	- grunt-premailer
	- grunt-domunger
	- grunt prettify
	- grunt-rename
	- grunt-nodemailer
- Mixture.io
	- .Net 4.5
- Ruby
	- Premailer
	- hpricot

## Supported Clients
- Gmail - 1st
- Outlook.com - 1st
- Yahoo - 1st
- Aol - 1st
- Android 4.4 - 1st
- Outlook App - 1st
- iOS Mail App - 1st
- Gmail App - 2nd
- Outlook 2007 - 3rd

## Build Process
- Convert to html in Mixture.io
- Run grunt test/deploy on target file

## Completed Todo's
- List styling
- Roles available table
- Left/Right sidebar
- Media q buttons 
- Fix stars in Yahoo.
- add 1px grey border to frame template.
- Pullquotes
- <hr> tag alternative, remove the need for 3 <td>
- Spacing for review stars
- links into alerts
- Media Object pattern
- grunt prettify
- package.json for grunt vars
- Rewrite GruntFile.js
- Break apart media queries
- box-shadow and border in hero template
- Just text template
- Update folder structure
- Restructure folders to css + less separate
- Media qs for specific templates
- External template
- Student newsletter template
- Outlook links via domunger
- Generic table
- hot jobs table
- icons
- latest vacancies and add to style guide
- Bullet points colours *Sparrow*
- latest vacancies premium
- hero with text overlay